
// Environment code for project testenv

import jason.asSyntax.*;
import jason.environment.*;
import java.util.logging.*;

public class Testenv extends Environment {

	private Logger logger = Logger.getLogger("testenv." + Testenv.class.getName());

	/** Called before the MAS execution with the args informed in .mas2j */
	@Override
	public void init(String[] args) {
		super.init(args);
		addPercept(Literal.parseLiteral("percept(demo)"));
	}

	@Override
	public boolean executeAction(String agName, Structure action) {
		if (action.getFunctor().equals("burn")) {
			addPercept(Literal.parseLiteral("fire"));
			return true;
		} 
		else if(action.getFunctor().equals("run")){
			logger.info("running!!!!");
			return true;
		}	
		else {
			logger.info("executing: " + action + ", but not implemented!");
			return false;
		}
	}

	/** Called before the end of MAS execution */
	@Override
	public void stop() {
		super.stop();
	}
}
