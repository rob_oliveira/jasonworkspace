
/*

Very simple vacuum cleaner agent in a world that has only four locations.

Perceptions:
. dirty: the current location has dirty
. clean: the current location is clean
. pos(X): the agent position is X (0 < X < 5).

Actions:
. suck: clean the current location
. left, right, up, down: move the agent

*/

// TODO: the code of the agent


!start.
!clean.
/* Plans */

+dirty : not clean <- suck; !clean.
+!clean : pos(1) <- right.
+!clean : pos(2) <- down.
+!clean : pos(3) <- up.
+!clean : pos(4) <- left.
//+pos(1) : clean <- right.
//+pos(2) : clean <- down.
//+pos(3) : clean <- up.
//+pos(4) : clean <- left.
