// Agent tom in project hello

/* Initial beliefs and rules */

/* Initial goals */

!start.

/* Plans */

+!start : true <- .send(rob, tell, hello).


+hello[source(A)] <- 
	.print("recebi um oi de ", A);
	.send(A, tell, hello).