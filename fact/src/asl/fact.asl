// Agent sample_agent in project fact

/* Initial beliefs and rules */

/* Initial goals */

!print_fact(5).

/* Plans */

+!print_fact(N) 
	<- !fact(N, Y) ;
	print("fatorial de ", N, " é: ", Y).  

+!fact(N, 1) : N == 0.

+!fact(N, Y) : 	N > 0 
        <- !fact(N-1, F);
        Y = N * F.
